<?php

namespace App\Controller;

use App\Client\Pizzeria;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PizzaController extends AbstractController
{
    /**
     * @Route("/", name="pizza")
     *
     * @param Pizzeria $pizzeria
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Pizzeria $pizzeria)
    {
        foreach (['fromage', 'poivrons', 'fruitsDeMer', 'vegetarienne'] as $type) {
            $pizzeria->commanderPizza($type);
        }

        return new Response();
    }
}
