<?php

namespace App\Factory;

use App\Entity\PizzaInterface;

class SimpleFabriqueDePizzas
{
    protected iterable $pizzas;

    public function __construct(iterable $pizzas)
    {
        $this->pizzas = $pizzas;
    }

    public function creerPizza(string $name): PizzaInterface
    {
        /** @var PizzaInterface $pizza */
        foreach ($this->pizzas as $pizza) {
            if ($pizza->isTypeMatch($name)) {
                return $pizza;
            }
        }

        throw new \Exception(sprintf('None pizza found for name %s', $name));
    }
}