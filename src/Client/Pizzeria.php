<?php

namespace App\Client;

use App\Entity\PizzaInterface;
use App\Factory\SimpleFabriqueDePizzas;

class Pizzeria
{
    /**
     * var SimpleFabriqueDePizzas
     */
    private $fabrique;

    public function __construct(SimpleFabriqueDePizzas $fabrique)
    {
        $this->fabrique = $fabrique;
    }

    public function commanderPizza(string $type): PizzaInterface
    {
        $pizza = $this->fabrique->creerPizza($type);

        $pizza->preparer();
        $pizza->cuire();
        $pizza->couper();
        $pizza->emballer();

        return $pizza;
    }

}