<?php

namespace App\Entity;

class PizzaFruitsDeMer extends AbstractPizza implements PizzaInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->setNom("Pizza Fruits de mer");
        $this->setPate("Pâte fine");
        $this->setSauce("White garlic sauce");
        $this->garnitures->add("Clams");
        $this->garnitures->add("Grated parmesan cheese");
    }

    public function isTypeMatch(string $type): bool
    {
        return $type === 'fruitsDeMer';
    }
}
