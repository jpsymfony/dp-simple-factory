<?php

namespace App\Entity;

class PizzaVegetarienne extends AbstractPizza implements PizzaInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->setNom("Pizza Végétarienne");
        $this->setPate("Crust");
        $this->setSauce("Sauce Marinara");
        $this->garnitures->add("Shredded mozzarella");
        $this->garnitures->add("Grated parmesan");
        $this->garnitures->add("Sliced mushrooms");
        $this->garnitures->add("Sliced red pepper");
        $this->garnitures->add("Sliced black olives");
    }

    public function isTypeMatch(string $type): bool
    {
        return $type === 'vegetarienne';
    }
}
