<?php

namespace App\Entity;

class PizzaFromage extends AbstractPizza implements PizzaInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->setNom("Pizza Fromage");
        $this->setPate("Pâte fine");
        $this->setSauce("Sauce Marinara");
        $this->garnitures->add("Parmesan");
        $this->garnitures->add("Mozarella");
    }

    public function isTypeMatch(string $type): bool
    {
        return $type === 'fromage';
    }
}
