<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

abstract class AbstractPizza
{
    public function __construct()
    {
        $this->garnitures = new ArrayCollection();
    }

    protected string $nom;

    protected string $pate;

    protected string $sauce;

    protected ArrayCollection $garnitures;

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     *
     * @return self
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return string
     */
    public function getPate(): string
    {
        return $this->pate;
    }

    /**
     * @param string $pate
     *
     * @return self
     */
    public function setPate(string $pate): self
    {
        $this->pate = $pate;

        return $this;
    }

    /**
     * @return string
     */
    public function getSauce(): string
    {
        return $this->sauce;
    }

    /**
     * @param string $sauce
     *
     * @return self
     */
    public function setSauce(string $sauce): self
    {
        $this->sauce = $sauce;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getGarnitures(): ArrayCollection
    {
        return $this->garnitures;
    }

    /**
     * @param ArrayCollection $garnitures
     *
     * @return self
     */
    public function setGarnitures(ArrayCollection $garnitures): self
    {
        $this->garnitures = $garnitures;

        return $this;
    }

    public function preparer(): void
    {
        echo "<p>Préparation de $this->nom<p/>";
        echo "<p>Etalage de la pâte...</p>";
        echo "<p>Ajout de la sauce...</p>";
        echo sprintf('<p>Ajout des garnitures: %s</p>', implode('-', $this->garnitures->toArray()));
    }

    public function cuire(): void
    {
        echo "<p>Cuisson 25 minutes à 180° C<p/>";
    }

    public function couper(): void
    {
        echo "<p>Découpage en parts triangulaires<p/>";
    }

    public function emballer(): void
    {
        echo "<p>Emballage dans une boîte officielle<p/>";
        echo "<hr>";
    }

    public abstract function isTypeMatch(string $name): bool;
}