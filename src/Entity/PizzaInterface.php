<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

interface PizzaInterface
{
    public function isTypeMatch(string $type): bool;

    public function getNom(): string;

    public function getPate(): string;

    public function getSauce(): string;

    public function getGarnitures(): ArrayCollection;

    public function preparer(): void;

    public function cuire(): void;

    public function couper(): void;

    public function emballer(): void;


}